﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using AssemblyCSharp;

public class PlayFabAddFriend : MonoBehaviour {

    public GameObject menuObject;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {


    }

    public void showMenu() {
        menuObject.GetComponent<Animator>().Play("ShowMenuAnimation");
    }

    public void hideMenu() {
        menuObject.GetComponent<Animator>().Play("hideMenuAnimation");
    }

    public void LeaveGame() {
        if (StaticStrings.showAdWhenLeaveGame)
            GameManager.Instance.adsScript.ShowAd();
        SceneManager.LoadScene("Menu");
        PhotonNetwork.BackgroundTimeout = 0;
        Debug.Log("Timeout 3");
        GameManager.Instance.cueController.removeOnEventCall();
        PhotonNetwork.LeaveRoom();

        GameManager.Instance.roomOwner = false;
        GameManager.Instance.resetAllData();

    }
}
