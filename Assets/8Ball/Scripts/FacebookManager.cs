﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using AssemblyCSharp;

public class FacebookManager : MonoBehaviour {

    private PlayFabManager playFabManager;
    public string name;
    public Sprite sprite;
    private GameObject FbLoginButton;
    private bool LoggedIn = false;
    private FacebookFriendsMenu facebookFriendsMenu;
    private bool alreadyGotFriends = false;
    public GameObject splashCanvas;
    public GameObject loginCanvas;
    public GameObject fbButton;
    public GameObject matchPlayersCanvas;
    public GameObject menuCanvas;
    public GameObject gameTitle;
    public GameObject idLoginDialog;
    public GameObject idRegisterDialog;
    public GameObject forgetPasswordDialog;


    public InputField loginEmail;
    public InputField loginPassword;
    public GameObject loginInvalidEmailorPassword;

    public InputField regiterEmail;
    public InputField registerPassword;
    public InputField registerNickname;
    public GameObject registerInvalidInput;

    public InputField resetPasswordEmail;
    public GameObject resetPasswordInformationText;



    void Start() {
        Debug.Log("FBManager start");
        GameManager.Instance.facebookManager = this;

        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        FbLoginButton = GameObject.Find("FbLoginButton");

        facebookFriendsMenu = GameManager.Instance.facebookFriendsMenu; //.GetComponent <FacebookFriendsMenu> ();
    }

    // Awake function from Unity's MonoBehavior
    void Awake() {
        Debug.Log("FBManager awake");
        GameManager.Instance.facebookManager = this;
        DontDestroyOnLoad(transform.gameObject);
        playFabManager = GameObject.Find("PlayFabManager").GetComponent<PlayFabManager>();
        if (!GameManager.Instance.logged) {

        }
    }

    private void OnHideUnity(bool isGameShown) {
        if (!isGameShown) {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        } else {
            // Resume the game - we're getting focus again
            Time.timeScale = 2;
        }
    }

    public void startRandomGame() {
        //		menuCanvas.SetActive (false);
        //		gameTitle.SetActive (false);

        GameManager.Instance.matchPlayerObject.GetComponent<SetMyData>().MatchPlayer();
        GameManager.Instance.matchPlayerObject.GetComponent<SetMyData>().setBackButton(true);
        //		matchPlayersCanvas.GetComponent <SetMyData> ().MatchPlayer ();
        //		matchPlayersCanvas.GetComponent <SetMyData> ().setBackButton (true);
        playFabManager.JoinRoomAndStartGame();
    }

    public void showRegisterDialog() {
        idLoginDialog.SetActive(false);
        idRegisterDialog.SetActive(true);
    }

    public void CloseLoginDialog() {
        loginInvalidEmailorPassword.SetActive(false);
        loginEmail.text = "";
        loginPassword.text = "";

        loginCanvas.SetActive(true);
        idLoginDialog.SetActive(false);
    }

    public void CloseRegisterDialog() {
        regiterEmail.text = "";
        registerPassword.text = "";
        registerNickname.text = "";
        registerInvalidInput.SetActive(false);
        loginCanvas.SetActive(true);
        idRegisterDialog.SetActive(false);
    }

    public void CloseForgetPasswordDialog() {
        resetPasswordEmail.text = "";
        resetPasswordInformationText.SetActive(false);
        forgetPasswordDialog.SetActive(false);
        loginCanvas.SetActive(true);
    }

    public void showForgetPasswordDialog() {
        forgetPasswordDialog.SetActive(true);
        idLoginDialog.SetActive(false);



    }

    public void IDLoginButtonPressed() {
        loginCanvas.SetActive(false);
        idLoginDialog.SetActive(true);
    }

    public IEnumerator loadImageMy(string url) {
        // Load avatar image

        // Start a download of the given URL
        WWW www = new WWW(url);

        // Wait for download to complete
        yield return www;


        GameManager.Instance.avatarMy = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f), 32);

    }

    public void destroy() {
        if (this.gameObject != null)
            DestroyImmediate(this.gameObject);
    }

    public void showLoadingCanvas() {
        loginCanvas.SetActive(false);
        splashCanvas.SetActive(true);
    }



}
