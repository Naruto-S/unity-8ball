﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using ExitGames.Client.Photon.Chat;
using AssemblyCSharp;

public class FacebookFriendsMenu : MonoBehaviour {

    public GameObject list;
    public GameObject friendPrefab;
    public GameObject friendPrefab2;
    public GameObject friendsMenu;
    public GameObject mainMenu;
    public InputField filterInputField;
    private PlayFabManager playFabManager;
    public GameObject confirmDialog;
    public GameObject confirmDialogText;
    public GameObject confirmDialogButton;

    private List<GameObject> friendsObjects = new List<GameObject>();
    // Use this for initialization
    void Start() {
        //		showFriendsTest ();
        playFabManager = GameManager.Instance.playfabManager;
    }

    // Update is called once per frame
    void Update() {

    }

    public void updateName(int i, string text, string id) {
        Debug.Log(i + " -- " + friendsObjects.Count);
        if (friendsObjects != null && friendsObjects.Count > 0 && i <= friendsObjects.Count - 1 && friendsObjects[i] != null) {
            friendsObjects[i].SetActive(true);
            friendsObjects[i].transform.Find("FriendName").GetComponent<Text>().text = text;
            // friendsObjects[i].transform.Find("FriendAvatar").gameObject.SetActive(true);
            // getFriendImageUrl(id, friendsObjects[i].transform.Find("FriendAvatar").GetComponent<Image>(), friendsObjects[i].transform.Find("FriendAvatar"));
        }

    }

    public void updateFriendStatus(int status, string id) {
        foreach (GameObject friend in friendsObjects) {
            if (friend.GetComponent<PlayFabFriendScript>().playfabID.Equals(id)) {
                if (status == ChatUserStatus.Online) {
                    friend.GetComponent<PlayFabFriendScript>().statusIndicatorText.GetComponent<Text>().text = "Online";
                    friend.GetComponent<PlayFabFriendScript>().statusIndicator.GetComponent<Image>().color = Color.green;
                } else if (status == ChatUserStatus.Offline) {
                    friend.GetComponent<PlayFabFriendScript>().statusIndicatorText.GetComponent<Text>().text = "Offline";
                    friend.GetComponent<PlayFabFriendScript>().statusIndicator.GetComponent<Image>().color = Color.red;
                }
            }
        }
    }

    public void showFriends() {
        //		foreach (GameObject o in fbFriendsObjects) {
        //
        //		}
        //		friendsMenu.gameObject.SetActive (true);
        //		mainMenu.gameObject.SetActive (false);
        //		GameObject.Find ("FriendsMask").GetComponent <ScrollRect>().normalizedPosition = new Vector2(0, 1);
    }

    public void showFriends(List<string> friendsNames, List<string> friendsIDs, List<string> friendsAvatars) {


        friendsMenu.gameObject.SetActive(true);
        mainMenu.gameObject.SetActive(false);



        if (friendsNames != null) {
            for (int i = 0; i < friendsNames.Count; i++) {


                GameObject friend = Instantiate(friendPrefab, Vector3.zero, Quaternion.identity) as GameObject;
                friend.transform.Find("FriendName").GetComponent<Text>().text = friendsNames[i];

                string friendID = friendsIDs[i];






                friend.transform.Find("InviteFriendButton").GetComponent<Button>().onClick.RemoveAllListeners();



                friend.GetComponent<MonoBehaviour>().StartCoroutine(loadImage(friendsAvatars[i], friend.transform.Find("FriendAvatar").GetComponent<Image>()));


                friend.transform.parent = list.transform;
                friend.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);

                friendsObjects.Add(friend);
                Debug.Log("KUPA");
                for (int j = 0; j < GameManager.Instance.friendsStatuses.Count; j++) {

                    string[] friend1 = GameManager.Instance.friendsStatuses[j];
                    Debug.Log(friendID + "  " + friend1[0]);
                    if (friend1[0].Equals(friendID)) {
                        Debug.Log("Found FRIEND");
                        if (friend1[1].Equals("" + ChatUserStatus.Online))
                            GameManager.Instance.facebookFriendsMenu.updateFriendStatus(ChatUserStatus.Online, friendID);
                        break;
                    }
                }

            }
        }



        GameObject.Find("FriendsMask").GetComponent<ScrollRect>().normalizedPosition = new Vector2(0, 1);

    }

    public void AddFacebookFriend(string friendsNames, string friendsIDs, string friendsAvatars) {


        //		friendsMenu.gameObject.SetActive (true);
        //		mainMenu.gameObject.SetActive (false);

        if (friendsNames != null) {



            GameObject friend = Instantiate(friendPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            friend.transform.parent = list.transform;
            friend.transform.Find("FriendName").GetComponent<Text>().text = friendsNames;

            string friendID = friendsIDs;
            string friendName = friendsNames;
            friend.transform.Find("InviteFriendButton").GetComponent<Button>().onClick.RemoveAllListeners();





            friend.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);

            friendsObjects.Add(friend);
            friend.GetComponent<MonoBehaviour>().StartCoroutine(loadImage(friendsAvatars, friend.transform.Find("FriendAvatar").GetComponent<Image>()));


        }

        GameObject.Find("FriendsMask").GetComponent<ScrollRect>().normalizedPosition = new Vector2(0, 1);

    }

    public void hideFriends() {
        filterInputField.text = "";

        foreach (GameObject o in friendsObjects) {
            Destroy(o);
        }

        friendsMenu.gameObject.SetActive(false);
        mainMenu.gameObject.SetActive(true);
    }


    public void FilterFriends() {
        string search = filterInputField.text;
        for (int i = 0; i < friendsObjects.Count; i++) {
            if (friendsObjects[i].transform.Find("FriendName").GetComponent<Text>().text.Length > 0)
                friendsObjects[i].SetActive(true);
            if (!friendsObjects[i].transform.Find("FriendName").GetComponent<Text>().text.ToLower().Contains(search.ToLower())) {
                friendsObjects[i].SetActive(false);

            }
        }

        GameObject.Find("FriendsMask").GetComponent<ScrollRect>().normalizedPosition = new Vector2(0, 1);
        //Debug.Log (filterInputField.text);
    }

    public void ChallengeFriend(string id) {
        GameManager.Instance.facebookFriendsMenu.hideFriends();
        GameManager.Instance.inviteFriendActivated = true;
        GameManager.Instance.challengedFriendID = id;
        GameManager.Instance.initMenuScript.showSelectTableScene(true);

        //playFabManager.challengeFriend(id, "");
    }

    public IEnumerator loadImage(string url, Image image) {
        // Load avatar image

        // Start a download of the given URL
        WWW www = new WWW(url);

        // Wait for download to complete
        yield return www;


        image.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f), 32);

    }

    //	public void showFriends() { 
    //		mainMenu.gameObject.SetActive (false);
    //		friendsMenu.gameObject.SetActive (true);
    //
    //		for (int i=0; i<50; i++) {
    //			GameObject friend = Instantiate(friendPrefab, Vector3.zero ,Quaternion.identity) as GameObject;
    //			friend.transform.Find ("FriendName").GetComponent <Text>().text = "Name";
    //
    //			friend.transform.parent = list.transform;
    //			friend.GetComponent <RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);
    //
    //		}
    //
    //	}
}
