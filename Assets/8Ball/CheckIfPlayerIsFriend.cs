﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckIfPlayerIsFriend : MonoBehaviour {

    public GameObject AddFriendButton;
    public GameObject mainObject;
    // Use this for initialization
    void Start() {
        GameManager.Instance.smallMenu = mainObject;
        GameManager.Instance.friendButtonMenu = AddFriendButton;

        if (GameManager.Instance.offlineMode) {
            AddFriendButton.SetActive(false);
            mainObject.GetComponent<RectTransform>().sizeDelta = new Vector2(mainObject.GetComponent<RectTransform>().sizeDelta.x, 260.0f);
        }

    }

    // Update is called once per frame
    void Update() {

    }
}
