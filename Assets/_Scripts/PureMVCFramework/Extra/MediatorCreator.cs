﻿using System.Collections;
using System.Collections.Generic;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using UnityEngine;

public class MediatorCreator : Mediator {
	
	/// <summary>
	/// Constructs a new mediator with the specified name and view component
	/// </summary>
	/// <param name="mediatorName">The name of the mediator</param>
	/// <param name="viewComponent">The view component to be mediated</param>
	public MediatorCreator(IPanel ipanel)
	{
		m_mediatorName = ipanel.GetType().Name;
		m_viewComponent = ipanel;
		
		Facade.RegisterMediator(this);
	}
	
	
	/// <summary>
	/// List the <c>INotification</c> names this <c>Mediator</c> is interested in being notified of
	/// </summary>
	/// <returns>The list of <c>INotification</c> names </returns>
	public override IList<string> ListNotificationInterests()
	{
		return ((IPanel)m_viewComponent).ListNotificationInterests();
	}

	/// <summary>
	/// Handle <c>INotification</c>s
	/// </summary>
	/// <param name="notification">The <c>INotification</c> instance to handle</param>
	/// <remarks>
	///     <para>
	///        Typically this will be handled in a switch statement, with one 'case' entry per <c>INotification</c> the <c>Mediator</c> is interested in. 
	///     </para>
	/// </remarks>
	public override void HandleNotification(INotification notification)
	{
		((IPanel)m_viewComponent).HandleNotification(notification);
	}
}
