﻿using System.Collections.Generic;
using PureMVC.Interfaces;

public interface IPanel {

	
	/// <summary>
	/// List <c>INotification interests</c>
	/// </summary>
	/// <returns>An <c>IList</c> of the <c>INotification</c> names this <c>IMediator</c> has an interest in</returns>
	IList<string> ListNotificationInterests();
		
	/// <summary>
	/// Handle an <c>INotification</c>
	/// </summary>
	/// <param name="notification">The <c>INotification</c> to be handled</param>
	void HandleNotification(INotification notification);
	
	
}
